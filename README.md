# Sorting Algorithm Benchmarks

Sorting algorithm benchmarks i wrote out of curiosity (and for homework, ofc.)

# Algorithms Included
* Insertion Sort
* Selection Sort
* Bubble Sort

# Algorithms to be Included
* Merge Sort
* Quick Sort

Written with separation of concerns in mind.